const ua = {
    "back_to_services": "Повернутись до сервісів",
    "projects": "Проєкти",
    "about": "Про нас",
    "services": "Сервіси",
    "vlog": "Влог",
    "news": "Новини",
    "contacts": "Контакти",
    "all": "Всі",
    "check_project": "Дивитись <br> проєкт",
    "to_see": "Дивитись <br> галерею",
    "hide_gallery": "Приховати галерею",
    "our_people": "Наша <br> команда",
    "read_more": "Читати далі",
    "name": "Ім'я",
    "phone_number": "Номер телефону",
    "message": "Повідомлення",
    "send": "Надіслати",
    "play_with_us": "Зіграємо?",
    "oops": "Йой, щось пішло не по плану",
    "go_to_main": "На головну",
    "score": "Рахунок",
    "your_score": "Ваш рахунок",
    "game_over": "Гру завершено",
    "thank_you": "Дякую за ваше повідомлення",
    "oops_number": "Йой, введіть номер",
    "oops_number_full": "Йой, введіть номер повністю",
    "oops_email": "Йой, напишіть пошту",
    "oops_email_incorrect": "Йой, пошта некоректна",
    "oops_name": "Йой, напишіть ім'я",
    "running_line": "АIММ - це команда, яка втілює проєктні ідеї в життя.",
    "addres_test": "вулиця перемоги"
}



module.exports = ua;
