
const en = require("./locales/en.js");
const ru = require("./locales/ru.js");
const ua = require("./locales/ua.js");

// sitemapGenerator

export default {
  mode: 'universal',
  // mode: 'spa',
  /*
  ** Headers of the page
  */
  head: {
    htmlAttrs: {
      prefix: "og: http://ogp.me/ns#"
    },
    title: "Проектно-архитектурная компания — AIMM | AIMM",
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      // { hid: 'description', name: 'description', content: process.env.npm_package_description || '' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.png' }
    ]
  },
  /*
  ** Customize the progress-bar color
  */
  loading: { color: '#fff' },
  /*
  ** Global CSS
  */
  css: [
    '~/static/scss/main.scss',
    '~/static/scss/tetris.scss',
    '~/static/scss/swiper.min.css'
  ],
  /*
  ** Plugins to load before mounting the App
  */
  plugins: [
    { src: "~plugins/vue-scrollmagic.js", ssr: false },
    { src: "~plugins/mask.js", ssr: false },
    { src: "~plugins/vue-check.js" },
    { src: './plugins/router.js', ssr: false },
    { src: './plugins/i18n.js' },
    { src: './plugins/axios.js' },
    // { src: './plugins/smoothScroll.js', ssr: false }
  ],
  /*
  ** Nuxt.js dev-modules
  */
  buildModules: [
  ],
  /*
  ** Nuxt.js modules
  */
  modules: [
    '@nuxtjs/axios',
    '@nuxtjs/pwa',
    'nuxt-lazy-load',
    // '@nuxtjs/amp',
    ['@nuxtjs/robots', {
      UserAgent: '*',
      Disallow: '/'
    }],
    ['nuxt-env', {
      keys: [
        { key: 'baseUrl', default: 'https://aimm-group.com' },
        // https://aimm-group.com
      ]
    }],
    ['nuxt-i18n', {
      locales: ["en", "ru", "ua"],
      defaultLocale: 'ua',
      vueI18n: {
        fallbackLocale: 'ua',
        messages: {
          ua,
          en,
          ru
        }
      },
      detectBrowserLanguage: false
    }],
    '@nuxtjs/sitemap'
  ],
  axios: {
    baseURL: 'https://api.aimm-group.com/!'
  },
  server: {
    port: 3010,
    //host: "0.0.0.0"
  },
  build: {
    extend(config, ctx) {
    }
  }
}
