const math = {
	lerp: (a, b, n) => {
		return (1 - n) * a + n * b;
	}
};

class Cursor {
	constructor() {
		this.setHovers()

		this.data = {
			mouse: {
				x: 0,
				y: 0
			},
			current: {
				x: 0,
				y: 0
			},
			last: {
				x: 0,
				y: 0
			},
			ease: 0.15,
			dist: 100,
			fx: {
				diff: 0,
				acc: 0,
				velo: 0,
				scale: 0
			},
			deg: 0
		};

		this.bounds = {
			h: 0,
			a: 0
		};

		this.rAF = null;
		this.targets = null;

		this.run = this.run.bind(this);
		this.mousePos = this.mousePos.bind(this);
		this.stick = this.stick.bind(this);

		this.state = {
			stick: false
		};

		this.init();

	}

	setHovers() {
		this.el = document.querySelector("[data-cursor]");
		this.stickies = document.querySelectorAll("[data-cursor-is-visible]");
		this.hidden = document.querySelectorAll("[data-cursor-is-hidden]");
		this.absoluted = document.querySelectorAll("[data-cursor-is-absoluted]");
		this.galery = document.querySelectorAll("[data-cursor-in-galery]");

	}

	updateHovers() {
		this.setHovers();
		this.addListeners();
	}

	mousePos(e) {
		this.data.mouse.x = e.clientX;
		this.data.mouse.y = e.clientY;

		this.data.current.x = e.clientX;
		this.data.current.y = e.clientY;


		if (this.el.classList.contains("cursor-absoluted")) {
			this.data.mouse.x = window.innerWidth/2 - 152;
			this.data.mouse.y = window.innerHeight/2;
	
			this.data.current.x = window.innerWidth/2 - 152;
			this.data.current.y = window.innerHeight/2;
		}
	}

	stick(target) { }

	run() {
		this.data.last.x = math.lerp(
			this.data.last.x,
			this.data.current.x,
			this.data.ease
		);
		this.data.last.y = math.lerp(
			this.data.last.y,
			this.data.current.y,
			this.data.ease
		);

		this.data.deg += 0.8;

		this.data.fx.diff = this.data.current.x - this.data.last.x;
		this.data.fx.acc = this.data.fx.diff / window.innerWidth;
		this.data.fx.velo = +this.data.fx.acc;
		this.data.fx.scale = 1 - Math.abs(this.data.fx.velo * 5);


		this.el.style.transform = `
        translate3d(${this.data.last.x}px, ${this.data.last.y}px, 0)
		scale(${this.data.fx.scale})
        `;

		this.raf();
	}

	raf() {
		this.rAF = requestAnimationFrame(this.run);
	}

	addListeners() {
		window.addEventListener("mousemove", this.mousePos, { passive: true });
		for (const element of this.stickies) {
			element.addEventListener("mouseenter", () => {
				this.el.classList.add("is-visible");
			});
			element.addEventListener("mouseleave", () => {
				this.el.classList.remove("is-visible");
			});
		}
		for (const element of this.hidden) {
			element.addEventListener("mouseenter", () => {
				this.el.classList.add("is-hidden");
			});
			element.addEventListener("mouseleave", () => {
				this.el.classList.remove("is-hidden");
			});
		}
		for (const element of this.absoluted) {
			element.addEventListener("mouseenter", () => {
				this.el.classList.add("cursor-absoluted");
			});
			element.addEventListener("mouseleave", () => {
				this.el.classList.remove("cursor-absoluted");
			});
		}
		for (const element of this.galery) {
			element.addEventListener("mouseenter", () => {
				this.el.classList.add("in-galery");
			});
			element.addEventListener("mouseleave", () => {
				this.el.classList.remove("in-galery");
			});
		}
	}

	init() {
		this.addListeners();
		this.raf();
	}
}

export default Cursor;
