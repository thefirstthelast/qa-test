export default /* glsl */ `
precision highp float;

uniform sampler2D uTexture;
uniform sampler2D uDispMap;
uniform float uDispScale;

varying vec2 vUv;
varying float vDisp;

void main() {
	vec2 pos = vUv;
  vec4 texture = texture2D(uTexture, pos);
	vec4 color = vec4(texture);
	// color.rgb *= vDisp;
	gl_FragColor = color;
}
`;
