export default /* glsl */ `
precision highp float;

uniform sampler2D uDispMap;
uniform float uDispScale;

varying vec2 vUv;
varying float vDisp;

void main() {
	// Normalized UV
	vUv = uv;
	vec3 pos = position;
	float disp = texture2D(uDispMap, vUv).g;
	disp *= uDispScale;
	vDisp = disp;

	pos.z += 300.0 * disp;

	vec4 modelViewPosition = modelViewMatrix * vec4(pos, 1.0);
	gl_Position = projectionMatrix * modelViewPosition;
}
`;
