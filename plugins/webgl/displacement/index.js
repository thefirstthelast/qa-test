import * as THREE from 'three';

import  { clamp, degToRad, lerp } from '~/plugins/utils';

export default class Displacement {
	constructor() {
		this._onScroll = this._onScroll.bind(this);
		this._tick = this._tick.bind(this);
	}

	get renderer() {
		return this._renderer;
	}

	init(props) {
		this._canvas = props.canvas;
		this._vertexShader = props.vertexShader;
		this._fragmentShader = props.fragmentShader;
		this._scale = props.scale;
		this._transformY = 0;
		this._extraMarginTop = props.extraMarginTop || 0;
		this._align = props.align;
		this._topTreshold = props.topTreshold;
		this._bottomTreshold = props.bottomTreshold;

		this._shouldRender = true;
		this._imageWidth = 0.5 * props.textureWidth;
		this._imageHeight = this._imageWidth * props.textureHeight / props.textureWidth;
				
		this._angleY = degToRad(props.angleY);
		this._angleX = degToRad(props.angleX);
		this._planeX = 0;
		this._planeY = 0;
		this._planeZ = 0;
		this._cameraZ = window.innerWidth < 425 ? 1075 : 1050;

		this._vars = {
			angleY: this._angleY,
			angleX: this._angleX,
			planeX: this._planeX,
			planeY: this._planeY,
			planeZ: this._planeZ
		};


		this._scroll = {dest: 0, pos: 0};
		this._deltaY = 0;
		this._time = 0;
		this._textures = [];
		
		this._computeBoundingBox();
		this._initWebgl();
		this._addEvents();
	}
	
	loadImages(images) {
		const highRes = window.devicePixelRatio > 1.5 && window.innerWidth > 700;
		const promises = [];
		
		images.map(i => {
			promises.push(this._loadImage({
				id: i.id, src: highRes ? i.src : i.src2x
			}));
		});
		
		return Promise.all(promises)
	}
	
	start() {
		this._createPlane();
		this._tick();
	}
	
	resize() {
		this._computeBoundingBox();
		this._windowWidth = window.innerWidth;
		this._windowHeight = window.innerHeight;
		this._width = this._canvas.parentElement.offsetWidth;
		// this._width =  window.innerWidth;
    this._height = this._windowWidth < 425 ? this._imageHeight / this._imageWidth * this._width : 0.66 * this._width;
		this._camera.aspect = this._width/this._height;
		this._camera.updateProjectionMatrix();
		this._renderer.setSize(this._width, this._height);
	}
	
	destroy() {
		this._isDestroyed = true;
		this._textures = [];
		this._scene.dispose();
	}

	setScale(val) {
		this._scale = val;
	}

	setTransformY(val) {
		this._transformY = val;
	}
	
	_computeBoundingBox() {

		const b = this._canvas.parentElement.getBoundingClientRect();

		this._boundingBox = {
			top: (b.top + window.pageYOffset),
			bottom: (b.bottom + window.pageYOffset),
			left: b.left,
			right: b.right
		};			
	}
	
	_addEvents() {
		window.addEventListener('scroll', this._onScroll);
	}
	
	_removeEvents() {
		window.removeEventListener('scroll', this._onScroll);
	}

	_onScroll() {
		this._scroll.dest = window.pageYOffset;
	}
	
	_createPlane() {
		const map = this._textures.find(t => t.id === 'texture').texture;
		const displacementMap = this._textures.find(t => t.id === 'displacement') ? this._textures.find(t => t.id === 'displacement').texture : null;
		
		const geo = new THREE.PlaneBufferGeometry(this._imageWidth, this._imageHeight, 100, 100);
		
		const uniforms = {
			uTexture: {type: 't', value: map},
			uDispMap: {type: 't', value: displacementMap},
			uDispScale: {type: 'f', value: 1},
			uTime: {type: 'f', value: this._time}
		};

		const mat = new THREE.ShaderMaterial({
			uniforms,
			vertexShader: this._vertexShader,
			fragmentShader: this._fragmentShader,
		});

		const mesh = new THREE.Mesh(geo, mat);
		const parent = new THREE.Object3D();
		parent.position.x = this._planeX;
		parent.position.y = this._planeY;
		parent.position.z = this._planeZ;
		mesh.rotation.set(this._angleX, 0, 0);
		parent.rotation.set(0, this._angleY, 0);
		parent.add(mesh);

		this._plane = {
			mesh,
			parent
		};

		this._scene.add(parent)
	}

	_loadImage(data) {
		return new Promise(resolve => {
			var loader = new THREE.TextureLoader()
			loader.load(data.src, texture => {
				texture.anisotropy = this._renderer.capabilities.getMaxAnisotropy();
				texture.needsUpdate = true;
				this._textures.push({id: data.id, texture})
				resolve();
			})
		})
	}

	_initWebgl() {
		this._windowWidth = window.innerWidth;
		this._windowHeight = window.innerHeight;
		this._width = this._canvas.parentElement.offsetWidth;
    this._height = this._windowWidth < 425 ? this._imageHeight / this._imageWidth * this._width : 0.66 * this._width;

    this._scene = new THREE.Scene();
		this._camera = new THREE.PerspectiveCamera(45, this._width / this._height, 0.1, 10000);
		this._camera.position.set(0, 0, this._cameraZ);

    this._scene.add(this._camera);

    this._renderer = new THREE.WebGLRenderer({
      canvas: this._canvas,
			antialias: false,
			alpha: true,
    });

		// TODO: enable that line to show verbose errors
    // this._renderer.debug.checkShaderErrors = true;
    this._renderer.setPixelRatio(window.devicePixelRatio > 1.5 ? 1.5 : 1);
		this._renderer.setSize(this._width, this._height);
	}

	_update() {
		if (this._isDestroyed) {
			return;
		}

		this._scroll.pos = lerp(this._scroll.pos, this._scroll.dest, 0.2);
		this._deltaY = this._boundingBox.top * this._scale - this._scroll.pos + this._transformY;
		const deltaYBottom = this._boundingBox.bottom * this._scale - this._scroll.pos + this._transformY;
		
		if (deltaYBottom > 0 && this._deltaY < 1.5 * this._height) {
			this._shouldRender = true;
		} else {
			this._shouldRender = false;
		}

		this._time += 0.01;

		this._updatePlane();
	}

	_updatePlane() {
    const viewportHeight = (window.innerHeight || document.documentElement.clientHeight)
    const tresholdTop = this._topTreshold * viewportHeight * this._scale;
    const tresholdBottom = this._bottomTreshold * viewportHeight * this._scale;
    const pos = this._canvas.parentElement.getBoundingClientRect().y
    const displacementScrollArea = 1 - ((viewportHeight - pos) / (tresholdTop - tresholdBottom))
    const intensity = Math.max(0, Math.min(1, displacementScrollArea))

		this._plane.mesh.rotation.x = intensity * this._vars.angleX;
		this._plane.parent.rotation.y = intensity * this._vars.angleY;

		this._plane.mesh.material.uniforms.uDispScale.value = intensity;
		this._plane.mesh.material.uniforms.uDispScale.needsUpdate = true;

		this._plane.mesh.material.uniforms.uTime.value = this._time;
		this._plane.mesh.material.uniforms.uTime.needsUpdate = true;
	}

	_render() {
		if (this._isDestroyed) {
			return;
		}

		this._renderer.render(this._scene, this._camera);
	}

	_tick() {
		requestAnimationFrame(this._tick);
		this._update();

		if (this._shouldRender) {
			this._render();
		}
	}
}