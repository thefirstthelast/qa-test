class Magnetic {
    constructor(magnet, magnet_area, strength) {
        this.magnet = magnet;
        this.magnet_area = magnet_area;
        this.strength = strength;


        this.magnet_area.addEventListener("mousemove", this.moveMagnet.bind(this));
        this.magnet_area.addEventListener("mouseout", (event) => {
            TweenMax.to(this.magnet, 1, {
                x: 0,
                y: 0,
                ease: Power4.easeOut
            });
        });
    }

    moveMagnet(event) {
        let magnetButton = this.magnet;
        let bounding = magnetButton.getBoundingClientRect();
        let buttonWidth = magnetButton.offsetWidth || magnetButton.getBBox().width;
        let buttonHeight = magnetButton.offsetHeight || magnetButton.getBBox().height;

        TweenMax.to(magnetButton, 1, {
            x:
                ((event.clientX - bounding.left) / buttonWidth - 0.5) *
                this.strength,
            y:
                ((event.clientY - bounding.top) / buttonHeight - 0.5) *
                this.strength,
            ease: Power4.easeOut
        });

        // magnetButton.style.transform = 'translate(' + (((( event.clientX - bounding.left)/(magnetButton.offsetWidth))) - 0.5) * strength + 'px,'+ (((( event.clientY - bounding.top)/(magnetButton.offsetHeight))) - 0.5) * strength + 'px)';

    }
}

export default Magnetic;