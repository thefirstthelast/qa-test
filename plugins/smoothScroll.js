export default function SmoothScroll(target, speed, smooth) {
  if (target === document)
    target =
      document.scrollingElement ||
      document.documentElement ||
      document.body.parentNode ||
      document.body // cross browser support for document scrolling

  var mooving_allow = true;
  var moving = false
  // var pos = target.scrollTop
  var pos = 0
  var frame =
    target === document.body && document.documentElement
      ? document.documentElement
      : target // safari is the new IE

  target.addEventListener('mousewheel', scrolled, { passive: false })
  target.addEventListener('DOMMouseScroll', scrolled, { passive: false })

  function scrolled(e) {
    e.preventDefault() // disable default scrolling
    if (mooving_allow) {


      var delta = normalizeWheelDelta(e)

      pos += -delta * speed
      pos = Math.max(
        0,
        Math.min(pos, target.scrollHeight - frame.clientHeight + 20)
      ) // limit scrolling

      if (!moving) update()
    }
  }

  function normalizeWheelDelta(e) {
    if (e.detail) {
      if (e.wheelDelta)
        return (e.wheelDelta / e.detail / 40) * (e.detail > 0 ? 1 : -1)
      // Opera
      else return -e.detail / 3 // Firefox
    } else return e.wheelDelta / 120 // IE,Safari,Chrome
  }

  this.setPosition = function (position) {
    // why +20? i don't know, but without +20 it not working corectly  
    pos = position + 20;
    update();
  }

  this.updatePosition = function (position) {
    // why +20? i don't know, but without +20 it not working corectly  
    pos += position;
    update();
  }

  this.stop = function () {
    mooving_allow = false;
  }

  this.start = function () {
    mooving_allow = true;
  }

  function update() {
    moving = true

    var delta = (pos - target.scrollTop) / smooth

    target.scrollTop += delta

    if (Math.abs(delta) > 0.5) requestFrame(update)
    else moving = false
  }


  this.destroy = function () {
    setTimeout(() => {
      pos = 0;
    }, 500);
    target.removeEventListener('mousewheel', scrolled, { passive: false })
    target.removeEventListener('DOMMouseScroll', scrolled, {
      passive: false
    })
  }

  var requestFrame = (function () {
    // requestAnimationFrame cross browser
    return (
      window.requestAnimationFrame ||
      window.webkitRequestAnimationFrame ||
      window.mozRequestAnimationFrame ||
      window.oRequestAnimationFrame ||
      window.msRequestAnimationFrame ||
      function (func) {
        window.setTimeout(func, 1000 / 50)
      }
    )
  })()
}