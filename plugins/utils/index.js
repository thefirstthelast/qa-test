export function lerp(a, b, n) { 
	return (1 - n) * a + n * b;
}

export function map(val, oldMin, oldMax, newMin, newMax) {
  return ((val - oldMin) * (newMax - newMin)) / (oldMax - oldMin) + newMin;
}

export function clamp(val, min, max) {
  return Math.min(Math.max(val, min), max);
}

export function degToRad(angle) {
  return Math.PI * angle / 180;
}

// Google gtag tracking
export function gtagTrack(e) {
  //  e.preventDefault()
  if (gtag) {
    gtag('event', 'click', {
      'event_category': e.currentTarget.getAttribute('data-gtag-category'),
      'event_label': e.currentTarget.getAttribute('data-gtag-label')
    });    
  }
}
