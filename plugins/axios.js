export default function ({ $axios, redirect }) {
    $axios.onError(err => {
        redirect('/error');
    })
}