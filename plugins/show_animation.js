import { TimelineMax, TweenLite, TimelineLite } from "gsap";
import SplitText from './gsap-bonus/SplitText'


TimelineMax.defaultOverwrite = false;
TweenLite.defaultOverwrite = false;

function showBlock(el, page) {
    var tween = new TimelineMax();
    let delay = 0;

    // if (window.innerWidth >= 640) {
    //     if (page && el.dataset.id % 2 === 1) {
    //         delay = 0.25;
    //     }
    //     if (!page) {
    //         if (el.dataset.id % 3 === 2) {
    //             delay = 0.5;
    //         }
    //         if (el.dataset.id % 3 === 1) {
    //             delay = 0.25;
    //         }
    //     }
    // }

    tween.fromTo(el, 0.8, { opacity: 0, y: 50 }, { opacity: 1, y: 0 }).delay(0.1);

    const scene = this.$scrollmagic
        .scene({
            triggerElement: el,
            duration: 0,
            reverse: false,
            // defaultOverwrite: false
        })
        //   .addIndicators({ name: "1 (duration: 0)" })
        .triggerHook(0.9)
        .setTween(tween);

    this.$scrollmagic.addScene(scene);
}

function showTitle(el) {
    var tween = new TimelineMax(),
        mySplitText = new SplitText(el, { type: 'lines' }),
        lines = mySplitText.lines //an array of all the divs that wrap each characterƒ
    tween.staggerFromTo(
        lines,
        0.75,
        { transformOrigin: "0 0", rotation: 5, opacity: 0 },
        { ease: Power4.easeOut, rotation: 0, opacity: 1 },
        0.35,
        '+=0'
    ).delay(0.2);
    // .staggerFromTo(lines, 2, { transformOrigin: "0 0", rotation: 5 }, { ease: Power4.easeOut, rotation: 0, immediateRender: false }, 0.7, '+=0');

    const scene = this.$scrollmagic
        .scene({
            triggerElement: el,
            duration: 0,
            reverse: false
        })
        //   .addIndicators({ name: "1 (duration: 0)" })
        .triggerHook(0.8)
        .setTween(tween);

    this.$scrollmagic.addScene(scene);
}


// function showText(el) {
//     var tween = new TimelineLite(),
//         mySplitText = new SplitText(el, { type: 'lines' }),
//         lines = mySplitText.lines //an array of all the divs that wrap each character

//     TweenLite.set(el)
//     // console.log(lines);

//     tween.staggerFrom(
//         lines,
//         1.2,
//         {
//             opacity: 0,
//             y: 20,
//             ease: Expo.easeOut
//         },
//         1.2 / lines.length,
//         '+=0',
//         // allDone
//     ).delay(0.2);

//     // function allDone() {
//     //     mySplitText.revert();
//     // }

//     const scene = this.$scrollmagic
//         .scene({
//             triggerElement: el,
//             duration: 0,
//             reverse: false
//         })
//         //   .addIndicators({ name: "1 (duration: 0)" })
//         .triggerHook(0.8)
//         .setTween(tween);

//     this.$scrollmagic.addScene(scene);
// }

function fadeAppear(el) {
    var tween = new TimelineMax();

    tween.fromTo(el, 0.8, { opacity: 0, y: 30 }, { opacity: 1, y: 0 });

    const scene = this.$scrollmagic
        .scene({
            triggerElement: el,
            duration: 0,
            reverse: false,
            // defaultOverwrite: false
        })
        //   .addIndicators({ name: "1 (duration: 0)" })
        .triggerHook(0.9)
        .setTween(tween);

    this.$scrollmagic.addScene(scene);
}

function fadeTitleAppear(el) {
    var tween = new TimelineMax();

    tween.fromTo(el, 0.8, { opacity: 0, transformOrigin: "0 0", rotation: 5 }, { ease: Power4.easeOut, opacity: 1, rotation: 0 });

    const scene = this.$scrollmagic
        .scene({
            triggerElement: el,
            duration: 0,
            reverse: false,
            // defaultOverwrite: false
        })
        //   .addIndicators({ name: "1 (duration: 0)" })
        .triggerHook(0.9)
        .setTween(tween);

    this.$scrollmagic.addScene(scene);
}


function _runAnimation() {

    let titles = document.querySelectorAll("h1, h2, h3, .h_one");
    for (const el of titles) {
        fadeTitleAppear.call(this, el);
    }

    let fade_appear_el = document.querySelectorAll(".js_fade_appear");
    for (const el of fade_appear_el) {
        fadeAppear.call(this, el);
    }


    let textes = document.querySelectorAll(".js_appear_animated_text");
    if (textes) {
        for (const el of textes) {
            if (el.nodeName == "DIV") {
                for (const p of el.querySelectorAll("p")) {
                    if (p) {
                        fadeAppear.call(this, p);
                    }
                }
            } else {
                fadeAppear.call(this, el);
            }
        }
    }

    if (process.client) {
        var simpleParallax = require("simple-parallax-js");
    }

    var images = document.querySelectorAll(".paralaxing");

    new simpleParallax(images);
}


function setAnimations() {
    let timeout = window.innerWidth <= 640 ? 200 : 100;
    // let timeout = 100;


    if (this.$store.state.from_name === null) {
        timeout += 500
    }
    this.$nextTick(() => {
        setTimeout(() => {
            _runAnimation.call(this);
        }, timeout);
    });
}




export { setAnimations };
export default showBlock;