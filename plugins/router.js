export default ({ app, store, redirect }) => {
    app.router.afterEach((to, from) => {
        store.commit("togglePageToggler");
        app.store.commit('setfromName', from.name);
        setTimeout(() => {
            document.body.classList.remove("lang_switching");
        }, 100);
        // console.log(to.path);
        // if (to.path === "/smi") {
        //     redirect('/news');
        // }
    });
};
