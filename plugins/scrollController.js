function disableScroll() {
    document.body.style.overflow = "hidden";
    document.documentElement.style.overflow = "hidden";
}

function enableScroll() {
    document.body.style.overflow = "auto";
    document.documentElement.style.overflow = "auto";
}

export default {enableScroll, disableScroll};