import { shapes, field } from "~/static/shapes";

const FIELD_HEIGHT = 20;
const FIELD_WIDTH = 10;
const FAST_SPEED = 20;
const SLOW_SPEED = 400;
const EMPTY_LINE = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0];


export default class Tetris {
    constructor() {
        this.is_in_game = false;
        this.game_over = false;
        this.shapes = shapes;
        this.current_shape = null;
        this.current_shape_position_x = 4;
        this.current_shape_position_y = 0;
        this.dot_size = 20;
        this.element_speed = SLOW_SPEED;
        this.moving_interval = null;
        this.shape_can_move = true;
        this.field = field;
        this.points = 0;
        this.stack_counter = 0;
    }

    stopMoving() {
        this.shape_can_move = false;

        console.log(this.field[0].join(","));

        console.log(this.current_shape.shape);
        let field_counter_before = 0;
        let field_counter_after = 0;
        let counter = 0;
        let field_copy = [];
        for (const i of this.field) {
            field_copy.push(i.slice(0));
        }

        for (const arr of field_copy) {
            for (const j of arr) {
                field_counter_before += j;
            }
        }

        for (let i = 0; i < this.current_shape.shape.length; i++) {
            for (let j = 0; j < this.current_shape.shape[i].length; j++) {
                if (
                    // this.current_shape_position_y <= 20 &&
                    // this.current_shape.shape[i] &&
                    this.current_shape.shape[i][j] === 1
                ) {
                    field_copy[this.current_shape_position_y + i - 1][
                        this.current_shape_position_x + j
                    ] = 1;
                    //   console.log(i )
                    //   this.field[19 + i - 1][
                    //     this.current_shape_position_x + j
                    //   ] = 1;
                    counter++;
                }
            }
        }

        for (const arr of field_copy) {
            for (const j of arr) {
                field_counter_after += j;
            }
        }

        if ((field_counter_after - field_counter_before !== counter) && this.stack_counter < 20) {
            console.log("wtf");
            this.stack_counter++;
            this.stopMoving();
            return;
        } else {
            this.field.length = 0;
            for (let i = 0; i < field_copy.length; i++) {
                this.field[i] = field_copy[i].slice(0);
            }
        }


        // console.log("afterStopMoving " + counter + "/" + (field_counter_after - field_counter_before));
        // console.log(this.field[0].join(","));

        for (let i = 0; i < this.field[0].length; i++) {
            if (this.field[0][i] === 1) {
                this.game_over = true;
                return;
            }
        }

        // clearInterval(this.moving_interval);
        this.destroyShape();
        this.destroyLines();
        this.fillField();
        this.current_shape_position_x = 4;
        this.current_shape_position_y = 0;
        this.createNewShape();
    }
    destroyLines() {
        console.log("destroyLines");
        console.log(this.field.length);

        for (let i = 0; i < this.field.length; i++) {
            if (this.field[i].indexOf(0) === -1) {
                for (let j = i; j >= 0; j--) {
                    if (this.field[j - 1]) {
                        this.field[j] = this.field[j - 1];
                    } else {
                        this.field[j] = EMPTY_LINE;
                    }
                }
                this.points += 10;
            }
        }

        for (let i = 0; i < this.field[0].length; i++) {
            if (this.field[0][i] === 1) {
                this.game_over = true;
            }
        }
        //       let a = Object.assign({}, this.field)
        // console.log(a);
    }
    startGame() {
        this.is_in_game = true;
        let tetris_wrapper = document.querySelector(".tetris_wrapper");
        tetris_wrapper.style.width = FIELD_WIDTH * this.dot_size + "px";
        tetris_wrapper.style.height = FIELD_HEIGHT * this.dot_size + "px";

        window.addEventListener("keydown", this.checkKey.bind(this));
        window.addEventListener("keyup", this.keyUp.bind(this));
        this.createNewShape();
    }
    createNewShape() {
        let shape_id = Math.floor(Math.random() * this.shapes.length);
        this.current_shape = this.shapes[shape_id];
        this.setShapePosition();
        this.drawNewShape();
        this.shape_can_move = true;
        this.moveNewShape();
    }
    drawNewShape() {
        let moving_shape_wrapper = document.querySelector(
            ".moving_shape_wrapper"
        );
        moving_shape_wrapper.style.width =
            this.current_shape.shape.length * this.dot_size + "px";

        for (let i = 0; i < this.current_shape.shape.length; i++) {
            for (let j = 0; j < this.current_shape.shape[i].length; j++) {
                if (this.current_shape.shape[i][j] !== 0) {
                    var new_dot = document.createElement("div");
                    new_dot.classList.add("dot_element");
                    new_dot.style.left = j * this.dot_size + "px";
                    new_dot.style.top = i * this.dot_size + "px";
                    moving_shape_wrapper.appendChild(new_dot);
                }
            }
        }
    }
    fillField() {
        let tetris_wrapper = document.querySelector(".tetris_wrapper");
        let dot_in_wrapper = document.querySelectorAll(
            ".tetris_wrapper>.dot_element"
        );
        for (let el of dot_in_wrapper) {
            el.remove();
        }
        for (let i = 0; i < this.field.length; i++) {
            for (let j = 0; j < this.field[i].length; j++) {
                if (this.field[i][j] === 1) {
                    var new_dot = document.createElement("div");
                    new_dot.classList.add("dot_element");
                    new_dot.style.left = j * this.dot_size + "px";
                    new_dot.style.top = i * this.dot_size + "px";
                    tetris_wrapper.appendChild(new_dot);
                }
            }
        }
    }
    setShapePosition() {
        let moving_shape_wrapper = document.querySelector(
            ".moving_shape_wrapper"
        );
        moving_shape_wrapper.style.left =
            this.current_shape_position_x * this.dot_size + "px";
        moving_shape_wrapper.style.top =
            this.current_shape_position_y * this.dot_size + "px";
    }
    checkMoving() {
        let can_to_move = true;
        for (let i = 0; i < this.current_shape.shape.length; i++) {
            for (let j = 0; j < this.current_shape.shape[i].length; j++) {
                if (this.current_shape.shape[i][j] === 1) {
                    if (
                        field[this.current_shape_position_y + i] &&
                        field[this.current_shape_position_y + i][
                        this.current_shape_position_x + j
                        ] === 1
                    ) {
                        can_to_move = false;
                    }
                }
            }
        }
        if (
            this.current_shape_position_y >=
            FIELD_HEIGHT - this.current_shape.shape.length + 1 ||
            !can_to_move
        ) {
            return false;
        }
        return true;
    }
    moveNewShape() {
        let moving_shape_wrapper = document.querySelector(
            ".moving_shape_wrapper"
        );

        if (this.moving_interval) {
            clearInterval(this.moving_interval);
        }

        this.moving_interval = setInterval(() => {
            this.current_shape_position_y++;
            if (!this.checkMoving()) {
                clearInterval(this.moving_interval);
                this.stopMoving();
                return;
            }
            moving_shape_wrapper.style.top =
                this.current_shape_position_y * this.dot_size + "px";
        }, this.element_speed);
    }
    changeSpeed() {
        if (this.shape_can_move) {
            // clearInterval(this.moving_interval);
            this.moveNewShape();
        }
    }
    moveDown() {
        if (
            this.current_shape_position_y <
            FIELD_HEIGHT - this.current_shape.shape.length
        ) {
            let moving_shape_wrapper = document.querySelector(
                ".moving_shape_wrapper"
            );
            this.current_shape_position_y++;
            moving_shape_wrapper.style.top =
                this.current_shape_position_y * this.dot_size + "px";
        }
    }
    destroyShape() {
        let moving_shape_wrapper = document.querySelectorAll(
            ".moving_shape_wrapper .dot_element"
        );
        for (let el of moving_shape_wrapper) {
            el.remove();
        }
    }
    rotateShape() {
        let rotated_shape = [];
        for (let i = 0; i < this.current_shape.shape[0].length; i++) {
            rotated_shape[i] = [];
        }
        for (let i = 0; i < this.current_shape.shape.length; i++) {
            for (let j = 0; j < this.current_shape.shape[i].length; j++) {
                rotated_shape[j][i] = this.current_shape.shape[i][j];
            }
        }
        for (let i = 0; i < rotated_shape.length; i++) {
            rotated_shape[i].reverse();
        }

        let current_shape_position_x = this.current_shape_position_x;

        while (rotated_shape[0].length > FIELD_WIDTH - current_shape_position_x) {
            current_shape_position_x--;
        }

        let can_rotate = true;

        // this.current_shape_position_x += this.current_shape.length;
        top: for (let i = 0; i < rotated_shape.length; i++) {
            for (let j = 0; j < rotated_shape[i].length; j++) {
                if (
                    rotated_shape[i][j] === 1 &&
                    field[this.current_shape_position_y + i][
                    current_shape_position_x + j
                    ] === 1
                ) {
                    can_rotate = false;
                    break top;
                }
            }
        }

        if (can_rotate) {
            this.current_shape_position_x = current_shape_position_x;
            this.current_shape.shape = rotated_shape;

            this.destroyShape();
            this.drawNewShape();
            this.setShapePosition();
        }
    }
    checkMoveToLeft() {
        for (let i = 0; i < this.current_shape.shape.length; i++) {
            for (let j = 0; j < this.current_shape.shape[i].length; j++) {
                if (this.current_shape.shape[i][j] === 1) {
                    // if (
                    //     field[this.current_shape_position_x] &&
                    //     field[this.current_shape_position_y + i][
                    //     this.current_shape_position_x + j - 1
                    //     ] === 1
                    // ) {
                    //     return false;
                    // }
                }
            }
        }
        return true;
    }
    checkMoveToRight() {
        for (let i = 0; i < this.current_shape.shape.length; i++) {
            for (let j = 0; j < this.current_shape.shape[i].length; j++) {
                if (this.current_shape.shape[i][j] === 1) {
                    // if (
                    //     field[this.current_shape_position_x + j + 1] &&
                    //     field[this.current_shape_position_y + i][
                    //     this.current_shape_position_x + j + 1
                    //     ] === 1
                    // ) {
                    //     return false;
                    // }
                }
            }
        }
        return true;
    }
    checkKey(e) {
        if (
            e.keyCode === 37 &&
            this.current_shape_position_x > 0 &&
            this.checkMoveToLeft()
        ) {
            this.current_shape_position_x--;
        }
        if (
            e.keyCode === 39 &&
            this.current_shape_position_x <
            FIELD_WIDTH - this.current_shape.shape[0].length &&
            this.checkMoveToRight()
        ) {
            this.current_shape_position_x++;
        }
        if (e.keyCode === 40) {
            e.preventDefault();
            this.element_speed = FAST_SPEED;
            this.changeSpeed();
        }
        if (e.keyCode === 38) {
            e.preventDefault();
        }
        this.setShapePosition();
    }
    keyUp(e) {
        if (e.keyCode === 40) {
            e.preventDefault();
            this.element_speed = SLOW_SPEED;
            this.changeSpeed();
        }
        if (e.keyCode === 38) {
            e.preventDefault();

            this.rotateShape();
        }
    }
}