import axios from "axios";
const languages = ["", "/en", "/ru"];
const static_routes = [
    {
        url: "/",
        changefreq: 'weekly',
        priority: 0.9,
    },
    {
        url: "/vlog",
        changefreq: 'daily',
        priority: 0.8,
    },
    {
        url: "/projects",
        changefreq: 'daily',
        priority: 0.8,
    },
    {
        url: "/news",
        changefreq: 'daily',
        priority: 0.8,
    },
    {
        url: "/contacts",
        changefreq: 'daily',
        priority: 0.8,
    },
    {
        url: "/services",
        changefreq: 'daily',
        priority: 0.8,
    },
    {
        url: "/about",
        changefreq: 'daily',
        priority: 0.8,
    }
]
let all_static_routes = [];

for (const lang of languages) {
    for (const route of static_routes) {
        let route_copy = Object.assign({}, route);
        route_copy.url = lang + route_copy.url;
        all_static_routes.push(route_copy);
    }
}

async function sitemapGenerator() {
    const projects = await axios.get('https://api.aimm-group.com/!/Fetch/page/all-projects');
    const all_news = await axios.get('https://api.aimm-group.com/!/Fetch/page/news');
    const services = await axios.get('https://api.aimm-group.com/!/Fetch/page/services');


    const projects_list_base = projects.data.data.children.map(project => ({
        url: `/projects/${project.page.slug}`,
        priority: 0.7,
        changefreq: 'weekly'
    }));
    const news_list_base = all_news.data.data.children.map(news => ({
        url: `/news/${news.page.slug}`,
        priority: 0.7,
        changefreq: 'weekly'
    }));
    const service_list_base = services.data.data.children.reduce((all_services, current_service) => {
        return all_services.concat(current_service.children.map(service_el => ({
            url: `/service/${current_service.page.slug}/${service_el.page.slug}`,
            priority: 0.7,
            changefreq: 'weekly'
        })));
    }, []);

    const all_routes_base = [...projects_list_base, ...news_list_base, ...service_list_base];
    let all_routes_full = [];


    for (const lang of languages) {
        for (const route of all_routes_base) {
            let route_copy = Object.assign({}, route);
            route_copy.url = lang + route_copy.url;
            all_routes_full.push(route_copy);
        }
    }
    return all_routes_full.concat(all_static_routes);
}
export default sitemapGenerator;