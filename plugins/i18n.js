export default function ({ app, store, route }) {
    app.i18n.beforeLanguageSwitch = (oldLocale, newLocale) => {
        store.commit("setAllNeedToUpdate");
        store.dispatch("getGlobals", newLocale);
        store.dispatch("getAllCategories", newLocale);
        store.dispatch("getNews", newLocale);
        store.dispatch("getServices", newLocale);
        store.dispatch("getContacts", newLocale);

        document.body.classList.add("lang_switching");
    }
}