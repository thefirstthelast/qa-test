export default class SimpleCursor {
    constructor(cursor, area) {
        this.cursor = cursor;
        this.area = area;
        this.clientX = 0;
        this.clientY = 0;

        TweenMax.to(this.cursor, 0, {
            opacity: 0
        });


        this.area.addEventListener("mousemove", e => {
            this.clientX = e.clientX;
            this.clientY = e.clientY;
        });

        this.area.addEventListener("mouseenter", e => {
            TweenMax.to(this.cursor, 0.3, {
                opacity: 1
            });
        });
        this.area.addEventListener("mouseleave", e => {
            TweenMax.to(this.cursor, 0.3, {
                opacity: 0
            });
        });

        const render = () => {
            TweenMax.set(this.cursor, {
                x: this.clientX,
                y: this.clientY
            });
            requestAnimationFrame(render);
        };
        requestAnimationFrame(render);
    }
}


