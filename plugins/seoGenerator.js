function seoLinkGenerator(base_link, link) {
    return [
        {
            rel: "alternate",
            href: base_link + link,
            hreflang: "ua"
        },
        {
            rel: "alternate",
            href: base_link + "/ru" + link,
            hreflang: "ru"
        },
        {
            rel: "alternate",
            href: base_link + "/en" + link,
            hreflang: "en"
        },
    ]
}


function seoMetaGenerator(og) {
    if (!og.image) {
        og.image = require("~/static/images/aimm_logo.png")
    }
    return [
        {
            hid: 'description',
            name: 'description',
            property: 'description',
            content: og.description
        },
        // open graph
        {
            name: 'og:image',
            property: 'og:image',
            content: og.image
        },
        {
            name: 'og:url',
            property: 'og:url',
            content: 'https://aimm-group.com' + og.url
        },
        {
            hid: 'og:title',
            name: 'og:title',
            property: 'og:title',
            content: og.title
        },
        {
            hid: 'og:description',
            name: 'og:description',
            property: 'og:description',
            content: og.description
        },
        {
            hid: 'og:name',
            name: 'og:name',
            property: 'og:name',
            content: "AIMM"
        }
    ]
}

export default seoLinkGenerator;
export  { seoLinkGenerator, seoMetaGenerator };
