// import TweenMax from "gsap/TweenMax.js";
// import initPageTransitions from "~/plugins/initPageTransitions";
import Swiper from "swiper";
import lazySizes from "lazysizes";

/**
 * demo.js
 * http://www.codrops.com
 *
 * Licensed under the MIT license.
 * http://www.opensource.org/licenses/mit-license.php
 *
 * Copyright 2019, Codrops
 * http://www.codrops.com
 */

class Demo2 {
	constructor(loopedSlides) {
		// initPageTransitions();
		this.loopedSlides = loopedSlides;
		this.initDemo();
		this.initSwiper();
		// window.lazySizes.init();
		lazySizes.init();
	}

	initDemo() {
		const { Back } = window;
		this.cursor = document.querySelector(".arrow-cursor");
		this.cursorIcon = document.querySelector(".arrow-cursor__icon");
		this.cursorBox = this.cursor.getBoundingClientRect();
		this.easing = Back.easeOut.config(1.7);
		this.animationDuration = 0.3;
		this.cursorSide = null; // will be "left" or "right"
		this.cursorInsideSwiper = false;
		this.swiperContainer = document.querySelector(".swiper-container");


		this.swiperButtonPrev = document.querySelector(".swiper-button-prev");
		this.swiperButtonNext = document.querySelector(".swiper-button-next");

		// initial cursor styling
		TweenMax.to(this.cursorIcon, 0, {
			rotation: -135,
			opacity: 0,
			scale: 0.5
		});

		document
			.getElementsByClassName("swiper-container")[0]
			.addEventListener("mousemove", e => {
				this.clientX = e.clientX;
				this.clientY = e.clientY;
			});

		const render = () => {
			TweenMax.set(this.cursor, {
				x: this.clientX,
				y: this.clientY
			});
			requestAnimationFrame(render);
		};
		requestAnimationFrame(render);

		// mouseenter
		this.onSwiperMouseEnter = e => {
			this.swiperBox = e.target.getBoundingClientRect();

			if (!this.clientX) this.clientX = e.clientX;
			if (!this.clientY) this.clientY = e.clientY;

			let startRotation;
			if (this.clientY < this.swiperBox.top + this.swiperBox.height / 2) {
				startRotation = -135;
			} else {
				startRotation =
					this.clientX > window.innerWidth / 2 ? 135 : -315;
			}
			TweenMax.set(this.cursorIcon, {
				rotation: startRotation
			});

			this.cursorSide =
				this.clientX > window.innerWidth / 2 ? "right" : "left";

			TweenMax.to(this.cursorIcon, this.animationDuration, {
				rotation: this.cursorSide === "right" ? 0 : -180,
				scale: 1,
				opacity: 1,
				ease: this.easing
			});
		};

		// mouseLeave
		this.onSwiperMouseLeave = e => {
			this.swiperBox = e.target.getBoundingClientRect();

			let outRotation = 0;
			if (this.clientY < this.swiperBox.top + this.swiperBox.height / 2) {
				outRotation = this.cursorSide === "right" ? -135 : -45;
			} else {
				outRotation = this.cursorSide === "right" ? 135 : -315;
			}

			TweenMax.to(this.cursorIcon, this.animationDuration, {
				rotation: outRotation,
				opacity: 0,
				scale: 0.3
			});

			this.cursorSide = null;
			this.cursorInsideSwiper = false;
		};

		// move cursor from left to right or right to left inside the Swiper
		this.onSwitchSwiperSides = () => {
			if (this.cursorInsideSwiper) {
				TweenMax.to(this.cursorIcon, this.animationDuration, {
					rotation: this.cursorSide === "right" ? -180 : 0,
					ease: this.easing
				});
				this.cursorSide = this.cursorSide === "left" ? "right" : "left";
			}

			if (!this.cursorInsideSwiper) {
				this.cursorInsideSwiper = true;
			}
		};



		this.swiperContainer.addEventListener("mouseenter", this.onSwiperMouseEnter);
		this.swiperContainer.addEventListener("mouseleave", this.onSwiperMouseLeave);

		this.swiperButtonPrev.addEventListener("mouseenter", this.onSwitchSwiperSides);
		this.swiperButtonNext.addEventListener("mouseenter", this.onSwitchSwiperSides);
	}

	initSwiper() {
		// const { Swiper } = window;
		console.log(this.loopedSlides)
		this.swiper = new Swiper(".swiper-container", {
			init: false,
			speed: 900,
			spaceBetween: 6,
			slidesPerView: "auto",
			loop: true,
			centeredSlides: true,
			observer: true,
			loopAdditionalSlides: this.loopedSlides,
			loopedSlides: this.loopedSlides,
			// preloadImages: true,
			// updateOnImagesReady: true,
			navigation: {
				nextEl: ".swiper-button-next",
				prevEl: ".swiper-button-prev"
			},
			breakpoints: {
				// when window width is >= 320px
				640: {
					spaceBetween: 8,
					loop: true,
					slidesPerView: "auto",
					centeredSlides: true
				}
			}
		});
		this.swiper.on("touchMove", e => {
			const { clientX, clientY } = e;
			this.clientX = clientX;
			this.clientY = clientY;

			this.cursorSide =
				this.clientX > window.innerWidth / 2 ? "right" : "left";

			TweenMax.to(this.cursorIcon, this.animationDuration, {
				rotation: this.cursorSide === "right" ? 0 : -180,
				ease: this.easing
			});
		});

		this.bumpCursorTween = TweenMax.to(this.cursor, 0.1, {
			scale: 0.85,
			onComplete: () => {
				TweenMax.to(this.cursor, 0.2, {
					scale: 1,
					ease: this.easing
				});
			},
			paused: true
		});

		this.swiper.on("slideChange", () => {
			this.bumpCursorTween.play();
		});
	}


	destroy() {
		this.swiperContainer.removeEventListener("mouseenter", this.onSwiperMouseEnter);
		this.swiperContainer.removeEventListener("mouseleave", this.onSwiperMouseLeave);

		this.swiperButtonPrev.removeEventListener("mouseenter", this.onSwitchSwiperSides);
		this.swiperButtonNext.removeEventListener("mouseenter", this.onSwitchSwiperSides);

		this.swiper.destroy(false, false);
	}
}

export default Demo2;
