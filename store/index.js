import Vue from 'vue'
import Vuex from 'vuex'
import services from "~/static/services";

Vue.use(Vuex);

const DEFAULT_CATEGORY = {
  key: "all",
  value: "all"
}

const store = () => new Vuex.Store({
  state: {
    baseLink: "https://api.aimm-group.com",
    page_toggler: false,
    is_third_column_light: false,
    is_fourth_column_light: false,
    mobile_alert_is_opened: false,
    from_name: null,
    is_preloader_visible: true,
    chosed_project_category: DEFAULT_CATEGORY,
    services,
    chosed_service: null,
    galery_distance: 0,
    /////// async data
    categories: null,
    main_page: null,
    main_page_projects: null,
    all_projects_page: null,
    all_projects_params: null,
    current_project: null,
    next_projects: null,
    about_page: null,
    news_page: null,
    current_new: null,
    next_news: null,
    services_page: null,
    current_service: null,
    contacts_page: null,
    vlog_page: null,
    globals: null,
    need_to_update: {}
  },
  mutations: {
    setNeedToUpdate(state, payload) {
      Vue.set(state.need_to_update, payload.page, payload.status);
    },
    togglePageToggler(state) {
      state.page_toggler = !state.page_toggler;
    },
    toggleThirdColumnLight(state, payload) {
      state.is_third_column_light = payload;
    },
    toggleFourthColumnLight(state, payload) {
      state.is_fourth_column_light = payload;
    },
    showMobileAlert(state) {
      state.mobile_alert_is_opened = true;
    },
    hideMobileAlert(state) {
      state.mobile_alert_is_opened = false;
    },
    setfromName(state, name) {
      state.from_name = name
    },
    setPreloaderVisible(state) {
      state.is_preloader_visible = false;
    },
    setProjectFilter(state, payload) {
      state.chosed_project_category = payload;
    },
    setSevice(state, payload) {
      state.chosed_service = payload;
    },
    setGaleryDistance(state, payload) {
      state.galery_distance = payload;
    },
    setCategories(state, payload) {
      state.categories = payload.map(item => ({
        key: item.slug,
        value: item.title
      }))
      // state.categories = [DEFAULT_CATEGORY].concat(categories);
    },
    setMainPageProjects(state, main_page) {
      state.main_page_projects = state.all_projects_page.children.filter((item) => {
        for (const main_page_id of main_page.projects) {
          if (main_page_id.id === item.page.id) {
            return true;
          }
        }
      })
      .map(item => item.page);
    },
    mutate(state, payload) {
      state[payload.property] = payload.with;
    },
    setAllNeedToUpdate(state, payload) {
      for (const key in state.need_to_update) {
        Vue.set(state.need_to_update, key, true)
      }
    }
  },
  actions: {
    async getMain({ state, commit }, locale) {
      if (state.need_to_update['main_page'] === undefined) {
        commit("setNeedToUpdate", { page: 'main_page', status: true })
      }
      if (state.need_to_update['main_page']) {
        const data = await this.$axios.$get(`/Fetch/page/home?deep=true&nested=false&locale=${locale}`);
        commit('setMainPageProjects', data.data);
        commit('mutate', {
          property: 'main_page',
          with: data.data
        }); 
        commit("setNeedToUpdate", { page: 'main_page', status: false })
      }
    },
    async getAllProjects({ state, commit }, locale) {
      if (state.need_to_update['all_projects_page'] === undefined) {
        commit("setNeedToUpdate", { page: 'all_projects_page', status: true })
      }
      if (state.need_to_update['all_projects_page']) {
        const data = await this.$axios.$get(`/Fetch/page/all-projects?locale=${locale}`);
        commit('mutate', {
          property: 'all_projects_page',
          with: data.data
        });
        commit("setNeedToUpdate", { page: 'all_projects_page', status: false })
      }
    },
    async getAllProjectsParams({ state, commit }, locale) {
      if (state.need_to_update['all_projects_params'] === undefined) {
        commit("setNeedToUpdate", { page: 'all_projects_params', status: true })
      }
      if (state.need_to_update['all_projects_params']) {
        const data = await this.$axios.$get(`/Fetch/page/all-projects?locale=${locale}&nested=false`);
        commit('mutate', {
          property: 'all_projects_params',
          with: data.data
        });
        commit("setNeedToUpdate", { page: 'all_projects_params', status: false })
      }
    },
    async getAllCategories({ state, commit }, locale) {
      if (state.need_to_update['categories'] === undefined) {
        commit("setNeedToUpdate", { page: 'categories', status: true })
      }
      if (state.need_to_update['categories']) {
        const data = await this.$axios.$get(`/Fetch/taxonomy/category?locale=${locale}`);
        commit('setCategories', data.data);
        commit("setNeedToUpdate", { page: 'categories', status: false })
      }
    },
    async getOneProject({ state, commit }, payload) {
      const data = await this.$axios.$get(`/Fetch/page/all-projects/${payload.path}?locale=${payload.locale}`);
      commit('mutate', {
        property: 'current_project',
        with: data.data
      });
    },
    async getNextProjects({ state, commit }, payload) {
      const data = await this.$axios.$get(`/related_data/projects/${payload.path}?locale=${payload.locale}`);
      commit('mutate', {
        property: 'next_projects',
        with: data.data
      });
    },
    async getAbout({ state, commit }, locale) {
      if (state.need_to_update['about_page'] === undefined) {
        commit("setNeedToUpdate", { page: 'about_page', status: true })
      }
      if (state.need_to_update['about_page']) {
        const data = await this.$axios.$get(`/Fetch/page/about?locale=${locale}`);
        commit('mutate', {
          property: 'about_page',
          with: data.data
        });
        commit("setNeedToUpdate", { page: 'about_page', status: false })

      }
    },
    async getNews({ state, commit }, locale) {
      if (state.need_to_update['news_page'] === undefined) {
        commit("setNeedToUpdate", { page: 'news_page', status: true })
      }
      if (state.need_to_update['news_page']) {
        const data = await this.$axios.$get(`/Fetch/page/news?locale=${locale}`);
        commit('mutate', {
          property: 'news_page',
          with: data.data
        });
        commit("setNeedToUpdate", { page: 'news_page', status: false })

      }
    },
    async getOneNew({ state, commit }, payload) {
      const data = await this.$axios.$get(`/Fetch/page/news/${payload.path}?locale=${payload.locale}`);
      commit('mutate', {
        property: 'current_new',
        with: data.data
      });
    },
    async getNextNews({ state, commit }, payload) {
      const data = await this.$axios.$get(`/related_data/articles/${payload.path}?locale=${payload.locale}`);
      commit('mutate', {
        property: 'next_news',
        with: data.data
      });
    },
    async getServices({ state, commit }, locale) {
      if (state.need_to_update['services_page'] === undefined) {
        commit("setNeedToUpdate", { page: 'services_page', status: true })
      }
      if (state.need_to_update['services_page']) {
        const data = await this.$axios.$get(`/Fetch/page/services?locale=${locale}`);
        commit('mutate', {
          property: 'services_page',
          with: data.data
        });
        commit("setNeedToUpdate", { page: 'services_page', status: false })
      }
    },
    async getOneService({ state, commit }, payload) {
      const data = await this.$axios.$get(`/Fetch/page/services${payload.path}?locale=${payload.locale}`);
      commit('mutate', {
        property: 'current_service',
        with: data.data
      });
    },
    async getContacts({ state, commit }, locale) {
      if (state.need_to_update['contacts_page'] === undefined) {
        commit("setNeedToUpdate", { page: 'contacts_page', status: true })
      }
      if (state.need_to_update['contacts_page']) {
        const data = await this.$axios.$get(`/Fetch/page/contacts?locale=${locale}`);
        commit('mutate', {
          property: 'contacts_page',
          with: data.data
        });
        commit("setNeedToUpdate", { page: 'contacts_page', status: false })
      }
    },
    async getVlog({ state, commit }, locale) {
      if (state.need_to_update['vlog_page'] === undefined) {
        commit("setNeedToUpdate", { page: 'vlog_page', status: true })
      }
      if (state.need_to_update['vlog_page']) {
        const data = await this.$axios.$get(`/Fetch/page/vlog?locale=${locale}`);
        commit('mutate', {
          property: 'vlog_page',
          with: data.data
        });
        commit("setNeedToUpdate", { page: 'vlog_page', status: false })
      }
    },
    async getGlobals({ state, commit }, locale) {
      if (state.need_to_update['globals'] === undefined) {
        commit("setNeedToUpdate", { page: 'globals', status: true })
      }
      if (state.need_to_update['globals']) {
        const data = await this.$axios.$get(`/Fetch/globals?locale=${locale}`);
        commit('mutate', {
          property: 'globals',
          with: data.data
        });
        commit("setNeedToUpdate", { page: 'globals', status: false })
      }
    }
  }
})

export default store