export default function ({ route, redirect }) {
    if (route.path[route.path.length - 1] === "/") {
        redirect(route.path.slice(0, route.path.length - 1));
    }
    if (route.path === "/smi" || route.path === "/blog") {
        redirect("/news");
    }
    if (route.path === "/opportunities") {
        redirect("/services");
    }
    if (route.path === "/ukr") {
        redirect("/");
    }
    const regexp = /.html$/;
    if (regexp.test(route.path)) {
        redirect(route.path.replace(regexp, ''));
    }
}