const services = [
    {
        id: 0,
        link: "Arcitecture",
        main_title: "Архитектурное проектирование общественных зданий",
        text:
            "Ac magna id odio volutpat, cursus bibendum est purus sem arcu elementum sit nam sit fausce euismod viverra. Magna id odio volutpat, cursus bibendum. Ac magna id odio volutpat, cursus bibendum id odio comagna id odio ",
        descriptions: [
            {
                id: 0,
                title: "Жилые комплексы",
                text:
                    "Ac magna id odio volutpat, cursus bibendum est purus sem arcu elementum sit nam sit fausce euismod viverra. Magna id odio volutpat, cursus bibendum. Ac magna id odio volutpat, cursus bibendum id odio comagna id odio "
            },
            {
                id: 1,
                title: "Жилые комплексы",
                text:
                    "Ac magna id odio volutpat, cursus bibendum est purus sem arcu elementum sit nam sit fausce euismod viverra. Magna id odio volutpat, cursus bibendum. Ac magna id odio volutpat, cursus bibendum id odio comagna id odio "
            },
            {
                id: 2,
                title: "Города и поселки",
                text:
                    "Ac magna id odio volutpat, cursus bibendum est purus sem arcu elementum sit nam sit fausce euismod viverra. Magna id odio volutpat, cursus bibendum. Ac magna id odio volutpat, cursus bibendum id odio comagna id odio "
            },
            {
                id: 3,
                title: "Дома и коттеджные городки",
                text:
                    "Ac magna id odio volutpat, cursus bibendum est purus sem arcu elementum sit nam sit fausce euismod viverra. Magna id odio volutpat, cursus bibendum. Ac magna id odio volutpat, cursus bibendum id odio comagna id odio "
            },
            {
                id: 4,
                title: "Жилые здания",
                text:
                    "Ac magna id odio volutpat, cursus bibendum est purus sem arcu elementum sit nam sit fausce euismod viverra. Magna id odio volutpat, cursus bibendum. Ac magna id odio volutpat, cursus bibendum id odio comagna id odio "
            }
        ]
    },
    {
        id: 1,
        main_title: "Архитектурное  общественных зданий",
        link: "Engineering",
        text:
            "Ac magna id odio volutpat, cursus bibendum est purus sem arcu elementum sit nam sit fausce euismod viverra. Magna id odio volutpat, cursus bibendum. Ac magna id odio volutpat, cursus bibendum id odio comagna id odio ",
        descriptions: [
            {
                id: 0,
                title: "Жилые комплексы",
                text:
                    "Ac magna id odio volutpat, cursus bibendum est purus sem arcu elementum sit nam sit fausce euismod viverra. Magna id odio volutpat, cursus bibendum. Ac magna id odio volutpat, cursus bibendum id odio comagna id odio "
            },
            {
                id: 1,
                title: "Жилые комплексы",
                text:
                    "Ac magna id odio volutpat, cursus bibendum est purus sem arcu elementum sit nam sit fausce euismod viverra. Magna id odio volutpat, cursus bibendum. Ac magna id odio volutpat, cursus bibendum id odio comagna id odio "
            },
            {
                id: 2,
                title: "Города и поселки",
                text:
                    "Ac magna id odio volutpat, cursus bibendum est purus sem arcu elementum sit nam sit fausce euismod viverra. Magna id odio volutpat, cursus bibendum. Ac magna id odio volutpat, cursus bibendum id odio comagna id odio "
            },
            {
                id: 3,
                title: "Дома и коттеджные городки",
                text:
                    "Ac magna id odio volutpat, cursus bibendum est purus sem arcu elementum sit nam sit fausce euismod viverra. Magna id odio volutpat, cursus bibendum. Ac magna id odio volutpat, cursus bibendum id odio comagna id odio "
            },
            {
                id: 4,
                title: "Жилые здания",
                text:
                    "Ac magna id odio volutpat, cursus bibendum est purus sem arcu elementum sit nam sit fausce euismod viverra. Magna id odio volutpat, cursus bibendum. Ac magna id odio volutpat, cursus bibendum id odio comagna id odio "
            }
        ]
    },
    {
        id: 2,
        main_title: "Архитектурное проектирование  зданий",
        link: "Master Planning",
        text:
            "Ac magna id odio volutpat, cursus bibendum est purus sem arcu elementum sit nam sit fausce euismod viverra. Magna id odio volutpat, cursus bibendum. Ac magna id odio volutpat, cursus bibendum id odio comagna id odio ",
        descriptions: [
            {
                id: 0,
                title: "Жилые комплексы",
                text:
                    "Ac magna id odio volutpat, cursus bibendum est purus sem arcu elementum sit nam sit fausce euismod viverra. Magna id odio volutpat, cursus bibendum. Ac magna id odio volutpat, cursus bibendum id odio comagna id odio "
            },
            {
                id: 1,
                title: "Жилые комплексы",
                text:
                    "Ac magna id odio volutpat, cursus bibendum est purus sem arcu elementum sit nam sit fausce euismod viverra. Magna id odio volutpat, cursus bibendum. Ac magna id odio volutpat, cursus bibendum id odio comagna id odio "
            },
            {
                id: 2,
                title: "Города и поселки",
                text:
                    "Ac magna id odio volutpat, cursus bibendum est purus sem arcu elementum sit nam sit fausce euismod viverra. Magna id odio volutpat, cursus bibendum. Ac magna id odio volutpat, cursus bibendum id odio comagna id odio "
            },
            {
                id: 3,
                title: "Дома и коттеджные городки",
                text:
                    "Ac magna id odio volutpat, cursus bibendum est purus sem arcu elementum sit nam sit fausce euismod viverra. Magna id odio volutpat, cursus bibendum. Ac magna id odio volutpat, cursus bibendum id odio comagna id odio "
            },
            {
                id: 4,
                title: "Жилые здания",
                text:
                    "Ac magna id odio volutpat, cursus bibendum est purus sem arcu elementum sit nam sit fausce euismod viverra. Magna id odio volutpat, cursus bibendum. Ac magna id odio volutpat, cursus bibendum id odio comagna id odio "
            }
        ]
    },
    {
        id: 3,
        main_title: " проектирование общественных зданий",
        link: "Restoration",
        text:
            "Ac magna id odio volutpat, cursus bibendum est purus sem arcu elementum sit nam sit fausce euismod viverra. Magna id odio volutpat, cursus bibendum. Ac magna id odio volutpat, cursus bibendum id odio comagna id odio ",
        descriptions: [
            {
                id: 0,
                title: "Жилые комплексы",
                text:
                    "Ac magna id odio volutpat, cursus bibendum est purus sem arcu elementum sit nam sit fausce euismod viverra. Magna id odio volutpat, cursus bibendum. Ac magna id odio volutpat, cursus bibendum id odio comagna id odio "
            },
            {
                id: 1,
                title: "Жилые комплексы",
                text:
                    "Ac magna id odio volutpat, cursus bibendum est purus sem arcu elementum sit nam sit fausce euismod viverra. Magna id odio volutpat, cursus bibendum. Ac magna id odio volutpat, cursus bibendum id odio comagna id odio "
            },
            {
                id: 2,
                title: "Города и поселки",
                text:
                    "Ac magna id odio volutpat, cursus bibendum est purus sem arcu elementum sit nam sit fausce euismod viverra. Magna id odio volutpat, cursus bibendum. Ac magna id odio volutpat, cursus bibendum id odio comagna id odio "
            },
            {
                id: 3,
                title: "Дома и коттеджные городки",
                text:
                    "Ac magna id odio volutpat, cursus bibendum est purus sem arcu elementum sit nam sit fausce euismod viverra. Magna id odio volutpat, cursus bibendum. Ac magna id odio volutpat, cursus bibendum id odio comagna id odio "
            },
            {
                id: 4,
                title: "Жилые здания",
                text:
                    "Ac magna id odio volutpat, cursus bibendum est purus sem arcu elementum sit nam sit fausce euismod viverra. Magna id odio volutpat, cursus bibendum. Ac magna id odio volutpat, cursus bibendum id odio comagna id odio "
            }
        ]
    },
    {
        id: 4,
        main_title: "  общественных зданий",
        link: "Interior design",
        text:
            "Ac magna id odio volutpat, cursus bibendum est purus sem arcu elementum sit nam sit fausce euismod viverra. Magna id odio volutpat, cursus bibendum. Ac magna id odio volutpat, cursus bibendum id odio comagna id odio ",
        descriptions: [
            {
                id: 0,
                title: "Жилые комплексы",
                text:
                    "Ac magna id odio volutpat, cursus bibendum est purus sem arcu elementum sit nam sit fausce euismod viverra. Magna id odio volutpat, cursus bibendum. Ac magna id odio volutpat, cursus bibendum id odio comagna id odio "
            },
            {
                id: 1,
                title: "Жилые комплексы",
                text:
                    "Ac magna id odio volutpat, cursus bibendum est purus sem arcu elementum sit nam sit fausce euismod viverra. Magna id odio volutpat, cursus bibendum. Ac magna id odio volutpat, cursus bibendum id odio comagna id odio "
            },
            {
                id: 2,
                title: "Города и поселки",
                text:
                    "Ac magna id odio volutpat, cursus bibendum est purus sem arcu elementum sit nam sit fausce euismod viverra. Magna id odio volutpat, cursus bibendum. Ac magna id odio volutpat, cursus bibendum id odio comagna id odio "
            },
            {
                id: 3,
                title: "Дома и коттеджные городки",
                text:
                    "Ac magna id odio volutpat, cursus bibendum est purus sem arcu elementum sit nam sit fausce euismod viverra. Magna id odio volutpat, cursus bibendum. Ac magna id odio volutpat, cursus bibendum id odio comagna id odio "
            },
            {
                id: 4,
                title: "Жилые здания",
                text:
                    "Ac magna id odio volutpat, cursus bibendum est purus sem arcu elementum sit nam sit fausce euismod viverra. Magna id odio volutpat, cursus bibendum. Ac magna id odio volutpat, cursus bibendum id odio comagna id odio "
            }
        ]
    },
    {
        id: 5,
        main_title: "Архитектурное проектирование",
        link: "Landscape architecture",
        text:
            "Ac magna id odio volutpat, cursus bibendum est purus sem arcu elementum sit nam sit fausce euismod viverra. Magna id odio volutpat, cursus bibendum. Ac magna id odio volutpat, cursus bibendum id odio comagna id odio ",
        descriptions: [
            {
                id: 0,
                title: "Жилые комплексы",
                text:
                    "Ac magna id odio volutpat, cursus bibendum est purus sem arcu elementum sit nam sit fausce euismod viverra. Magna id odio volutpat, cursus bibendum. Ac magna id odio volutpat, cursus bibendum id odio comagna id odio "
            },
            {
                id: 1,
                title: "Жилые комплексы",
                text:
                    "Ac magna id odio volutpat, cursus bibendum est purus sem arcu elementum sit nam sit fausce euismod viverra. Magna id odio volutpat, cursus bibendum. Ac magna id odio volutpat, cursus bibendum id odio comagna id odio "
            },
            {
                id: 2,
                title: "Города и поселки",
                text:
                    "Ac magna id odio volutpat, cursus bibendum est purus sem arcu elementum sit nam sit fausce euismod viverra. Magna id odio volutpat, cursus bibendum. Ac magna id odio volutpat, cursus bibendum id odio comagna id odio "
            },
            {
                id: 3,
                title: "Дома и коттеджные городки",
                text:
                    "Ac magna id odio volutpat, cursus bibendum est purus sem arcu elementum sit nam sit fausce euismod viverra. Magna id odio volutpat, cursus bibendum. Ac magna id odio volutpat, cursus bibendum id odio comagna id odio "
            },
            {
                id: 4,
                title: "Жилые здания",
                text:
                    "Ac magna id odio volutpat, cursus bibendum est purus sem arcu elementum sit nam sit fausce euismod viverra. Magna id odio volutpat, cursus bibendum. Ac magna id odio volutpat, cursus bibendum id odio comagna id odio "
            }
        ]
    },
    {
        id: 6,
        main_title: "   зданий",
        link: "Reconstruction",
        text:
            "Ac magna id odio volutpat, cursus bibendum est purus sem arcu elementum sit nam sit fausce euismod viverra. Magna id odio volutpat, cursus bibendum. Ac magna id odio volutpat, cursus bibendum id odio comagna id odio ",
        descriptions: [
            {
                id: 0,
                title: "Жилые комплексы",
                text:
                    "Ac magna id odio volutpat, cursus bibendum est purus sem arcu elementum sit nam sit fausce euismod viverra. Magna id odio volutpat, cursus bibendum. Ac magna id odio volutpat, cursus bibendum id odio comagna id odio "
            },
            {
                id: 1,
                title: "Жилые комплексы",
                text:
                    "Ac magna id odio volutpat, cursus bibendum est purus sem arcu elementum sit nam sit fausce euismod viverra. Magna id odio volutpat, cursus bibendum. Ac magna id odio volutpat, cursus bibendum id odio comagna id odio "
            },
            {
                id: 2,
                title: "Города и поселки",
                text:
                    "Ac magna id odio volutpat, cursus bibendum est purus sem arcu elementum sit nam sit fausce euismod viverra. Magna id odio volutpat, cursus bibendum. Ac magna id odio volutpat, cursus bibendum id odio comagna id odio "
            },
            {
                id: 3,
                title: "Дома и коттеджные городки",
                text:
                    "Ac magna id odio volutpat, cursus bibendum est purus sem arcu elementum sit nam sit fausce euismod viverra. Magna id odio volutpat, cursus bibendum. Ac magna id odio volutpat, cursus bibendum id odio comagna id odio "
            },
            {
                id: 4,
                title: "Жилые здания",
                text:
                    "Ac magna id odio volutpat, cursus bibendum est purus sem arcu elementum sit nam sit fausce euismod viverra. Magna id odio volutpat, cursus bibendum. Ac magna id odio volutpat, cursus bibendum id odio comagna id odio "
            }
        ]
    }
];

export default services;
