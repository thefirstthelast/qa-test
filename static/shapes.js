const shapes = [
    {
        id: 0,
        name: 'dot',
        shape: [[1]],
    },
    {
        id: 1,
        name: 'line',
        shape: [[1, 1, 1, 1]],
    },
    {
        id: 2,
        name: 'square',
        shape: [[1, 1], [1, 1]],
    },
    {
        id: 3,
        name: 'pimpa',
        shape: [[0, 1, 0], [1, 1, 1]],
    },
    {
        id: 4,
        name: 'g1',
        shape: [[1, 0, 0], [1, 1, 1]],
    },
    {
        id: 5,
        name: 'g2',
        shape: [[0, 0, 1], [1, 1, 1]],
    },
    {
        id: 6,
        name: 'z1',
        shape: [[0, 1, 1], [1, 1, 0]],
    },
    {
        id: 7,
        name: 'z2',
        shape: [[1, 1, 0], [0, 1, 1]],
    },
    // {
    //     id: 8,
    //     name: 'z22',
    //     shape: [[1, 1, 0], [0, 1, 1], [0, 1, 1]],
    // }
]

export default shapes;

let field = [];
for (let i = 0; i < 20; i++) {
    field[i] = [];
    for (let j = 0; j < 10; j++) {
        field[i][j] = 0;
    }
}

export {shapes, field};